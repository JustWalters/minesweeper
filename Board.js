'use strict';

const BoardSquare = require('./BoardSquare').BoardSquare;

class Board {
	constructor(size, numOfMines) {
		// Square States
		this.PRISTINE = 0;
		this.FLAGGED = 1;
		this.QUESTION = 2;
		this.REVEALED = -1;
		this.EXPLODED = -2;

		this.size = 5;
		this.numOfMines = 5;
		this.updateSettings(size, numOfMines);

		this.buildBoard();
	}

	buildBoard() {
		let i, j, row;
		this.board = [];

		for (i = 0; i < this.size; i++) {
			row = [];
			for (j = 0; j < this.size; j++) {
				row.push(new BoardSquare(this.PRISTINE));
			}
			this.board.push(row);
		}

		this.populateBoard();
		return this;
	}

	populateBoard() {
		let i, row, col, boardSquare;
		for (i = 0; i < this.numOfMines; i++) {
			row = Math.floor(Math.random() * this.size);
			col = Math.floor(Math.random() * this.size);

			boardSquare = this.board[row][col];

			if (boardSquare.isAMine()) { i--; }
			else {
				boardSquare.setIsAMine(true);
				this.updateMineNeighbors(row, col);
			}
		}
	}

	updateMineNeighbors(row, col) {
		let i, j, cols, sq;

		// For 9-square grid around this square, increase count
		for (i = -1; i <= 1; i++) {
			for (j = -1; j <= 1; j++) {
				if (i == 0 && j == 0) continue;

				cols = this.board[row + i];
				sq = cols && cols[col + j];
				if (!sq) continue;

				sq.increaseMineCount();
			}
		}
	}

	// reveals squares that don't have any mine neighbors
	updateEmptyNeighbors(row, col) {
		let startSq = this.board[row][col];
		if (startSq.getMineCount() !== 0) return;

		let i, j, cols, sq;
		// For 9-square grid around this square, check count
		for (i = -1; i <= 1; i++) {
			for (j = -1; j <= 1; j++) {
				if (i == 0 && j == 0) continue;

				cols = this.board[row + i];
				sq = cols && cols[col + j];

				if (sq && sq.getStatus() === this.PRISTINE && !sq.isAMine()) {
					sq.setStatus(this.REVEALED);
					if (sq.getMineCount() === 0) this.updateEmptyNeighbors(row + i, col + j);
				}
			}
		}
	}

	printBoard() {
		let str = '', status;

		this.iterOverSquares((sq, row, col) => {
			status = sq.getStatus();
			switch (status) {
				case this.PRISTINE:
					str += '[ ] ';
					break;
				case this.FLAGGED:
					str += '[🚩] ';
					break;
				case this.QUESTION:
					str += '[?] ';
					break;
				case this.REVEALED:
					str += `[${sq.getMineCount()}] `;
					break;
				case this.EXPLODED:
					str += '[💣] ';
					break;
				default:
					str += '[_] ';
			}

			// Add newline after last column
			if (col === this.size - 1) str += '\n';
		});

		console.log(str);
	}

	guessSquare(row, col, flag) {
		// Move from 1-index to 0-index and check bounds
		if (--row > this.size || row < 0 || --col > this.size || col < 0)
			throw new Error('Square out of bounds');

		let sq = this.board[row][col];

		if (flag === 'F') {
			sq.setStatus(this.FLAGGED);
		} else if (flag === 'P') {
			sq.setStatus(this.PRISTINE);
		} else if (flag === 'Q') {
			sq.setStatus(this.QUESTION);
		} else if (sq.isAMine()) {
			sq.setStatus(this.EXPLODED);
			return this.checkBoardState(this.EXPLODED);
		} else {
			sq.setStatus(this.REVEALED);
			this.updateEmptyNeighbors(row, col);
		}

		return this.checkBoardState();
	}

	// Returns message on game end, false otherwise
	checkBoardState(newStatus) {
		// If there is an EXPLODED square, game over
		// If all mines are FLAGGED and no other squares are, user wins
		// If all non-mine squares are REVEALED, user wins

		if (newStatus === this.EXPLODED) {
				return 'Game Over';
		}

		let unfoundMines = 0,
				wrongFlags = 0,
				checkedAllNonMines = true;

		let status, isAMine;

		this.iterOverSquares(sq => {
			status = sq.getStatus();
			isAMine = sq.isAMine();

			if (isAMine) {
				if (status !== this.FLAGGED) unfoundMines++;
			} else {
				if (status !== this.REVEALED) checkedAllNonMines = false;
				if (status === this.FLAGGED) wrongFlags++;
			}
		});

		if (!unfoundMines && !wrongFlags) return 'You Win';
		if (checkedAllNonMines) return 'You Win 2';

		return false;
	}

	updateSettings(boardSize, numOfMines) {
		if (boardSize && Number.isInteger(boardSize) && boardSize > 0) {
			this.size = boardSize;
		}

		if (numOfMines && Number.isInteger(numOfMines) && numOfMines > 0) {
			if (numOfMines >= (this.size * this.size)) {
				throw new Error(`You can't have ${numOfMines} mines on a ${this.size}x${this.size} board`);
			}
			this.numOfMines = numOfMines;
		}
	}

	iterOverSquares(cb) {
		let i, j, sq;
		for (i = 0; i < this.size; i++) {
			for (j = 0; j < this.size; j++) {
				sq = this.board[i][j];
				cb(sq, i, j);
			}
		}
	}
}

module.exports.Board = Board;
