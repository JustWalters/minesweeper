'use strict';

class BoardSquare {
	constructor(status) {
		this.count = 0;
		this._isAMine = false;
		this.setStatus(status);
	}

	isAMine() {
		return this._isAMine;
	}

	setIsAMine(isAMine) {
		this._isAMine = !!isAMine;
		return this._isAMine;
	}

	getStatus() {
		return this.status;
	}

	setStatus(status) {
		if (this.status < 0) return;
		this.status = status;
	}

	getMineCount() {
		return this.count;
	}

	increaseMineCount() {
		this.count++;
	}
}

module.exports.BoardSquare = BoardSquare;
