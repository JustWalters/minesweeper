# Minesweeper [ ![Codeship Status for JustWalters/minesweeper](https://app.codeship.com/projects/bcd17ea0-fd98-0135-0027-2eb877ffdf8a/status?branch=master)](https://app.codeship.com/projects/279272)

A small minesweeper game for the terminal.

## Running

Start the game with `node index.js`. Simple instructions will be printed to the terminal, where you can then enter your commands. To quit, you can type any of "quit", "exit", or "bye", or you can use your normal method of exting a running process.

## Testing

After installing mocha with `npm install`, run `npm test`.
