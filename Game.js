'use strict';

const readline = require('readline');
const Board = require('./Board').Board;

class Game {
	constructor() {
		this.boardSize = 5;
		this.numOfMines = 5;
		this.board = new Board(this.boardSize, this.numOfMines);
		this.score = 0;

		this.rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});

		process.on('SIGINT', () => {
			this.rl.close();
		});

		return this;
	}

	play() {
		console.log('Enter column number and row number, in that order, separated by a comma.');
		console.log('To flag a square, append an "f" to your selection. ex: 2,3f');
		console.log('To add a question mark to a square, append an "q" to your selection. ex: 2,3q');
		console.log('To unmark a square, append an "u" to your selection. ex: 2,3u');

		this.build();
		this.questionAsPromise('\nStart game?\n')
			.then(this.handleGameStartAnswer.bind(this));

		return this;
	}

	continueGame() {
		this.rl.question('Column and row?\n', answer => {
			let { row, col, flag, quit } = this.parseAnswer(answer);

			if (quit) return this.endGame();
			if (col === undefined || row === undefined) return this.continueGame();

			let res;
			try {
				res = this.board.guessSquare(row, col, flag);
			} catch (e) {
				console.log(e.message);
				return this.continueGame();
			}

			this.board.printBoard();

			if (res) {
				this.endGame(res);
			} else {
				this.continueGame();
			}
		});
	}

	parseAnswer(answer) {
		if (!answer) return {};
		answer = answer.trim();

		if (answer.match(/quit|exit|bye/ig)) return { quit: true };

		let coords = answer.split(',');
		if (!coords || coords.length !== 2) return {};

		let col = parseInt(coords[0]);
		let row = parseInt(coords[1]);

		if (isNaN(row) || isNaN(col)) return {};

		let flag = '';
		let lastChar = answer.charAt(answer.length-1);

		if (lastChar === 'f') flag = 'F';
		else if (lastChar === 'u') flag = 'P';
		else if (lastChar === 'q') flag = 'Q';

		return { col, row, flag };
	}

	endGame(message) {
		if (message) {
			console.log(message);
			this.questionAsPromise('\nPlay again?\n')
				.then(this.handleGameStartAnswer.bind(this));
			return;
		}

		console.log('Good bye');
		this.rl.close();
		process.exit(0);
	}

	handleGameStartAnswer(answer) {
		if (this.isAffirmative(answer)) {
			console.log(`The board is currently ${this.boardSize}x${this.boardSize} with ${this.numOfMines} hidden mines.`);
			this.questionAsPromise('Would you like to change the settings?\n')
				.then(changeSettings => {
					if (this.isAffirmative(changeSettings)) {
						return this.updateSettings();
					}
				})
				.then(() => {
					this.build();
					this.continueGame();
				});
		}
		else this.endGame();
	}

	updateSettings() {
		let sizeQ = 'Enter a new width for the board. Leave blank to use current setting\n';
		let numOfMinesQ = 'Enter the number of mines you want. Again, you can leave it blank\n';

		return this.questionAsPromise(sizeQ)
			.then(this.verifyInteger)
			.then(size => {
				if (size && size > 0) this.boardSize = size;
			})
			.then(() => {
				return this.questionAsPromise(numOfMinesQ);
			})
			.then(this.verifyInteger)
			.then(numOfMines => {
				if (numOfMines && numOfMines > 0 && numOfMines < (this.boardSize * this.boardSize))
					this.numOfMines = numOfMines;
			})
			.then(() => {
				this.board.updateSettings(this.boardSize, this.numOfMines);
			})
			.catch(err => {
				console.error('Something went wrong while updating the settings');
				console.error(err.message);
			});
	}

	isAffirmative(answer) {
		return answer && answer.match(/y(es)?/i);
	}

	questionAsPromise(question) {
		return new Promise(resolve => this.rl.question(question, resolve));
	}

	verifyInteger(potentialNum) {
		try {
			let num = Number(potentialNum);
			return Number.isInteger(num) ? num : false;
		} catch (e) {
			return false;
		}
	}

	build() {
		this.board.buildBoard();
	}
}

module.exports.Game = Game;
