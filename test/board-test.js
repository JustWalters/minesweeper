'use strict';

const assert = require('assert');
const Board = require('../Board').Board;

describe('Board', function() {
	describe('size and mine settings', function() {
		it('should have defaults', function() {
			let board = new Board();
			assert(board.size);
			assert(board.numOfMines);
		});

		describe('should be configurable', function() {
			it('on instantiation', function() {
				let size = 12;
				let numOfMines = 12;
				let board = new Board(size, numOfMines);

				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);
			});

			it('after instantiation', function() {
				let size = 12;
				let numOfMines = 12;
				let board = new Board();

				assert.notEqual(board.size, size);
				assert.notEqual(board.numOfMines, numOfMines);

				board.updateSettings(size, numOfMines);
				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);
			});
		});

		describe('should not accept invalid settings', function() {
			let zero, negative, stringNum, fractional, tooManyMines;

			before(function() {
				zero = 0;
				negative = -1;
				stringNum = '2';
				fractional = 5.5;
				tooManyMines = 100;
			});

			it('on instantiation', function() {
				let board;

				board = new Board(zero, zero);
				assert.notEqual(board.size, zero);
				assert.notEqual(board.numOfMines, zero);

				board = new Board(negative, negative);
				assert.notEqual(board.size, negative);
				assert.notEqual(board.numOfMines, negative);

				board = new Board(stringNum, stringNum);
				assert.notEqual(board.size, stringNum);
				assert.notEqual(board.numOfMines, stringNum);

				board = new Board(fractional, fractional);
				assert.notEqual(board.size, fractional);
				assert.notEqual(board.numOfMines, fractional);

				assert.throws(() => {
					board = new Board(2, tooManyMines);
				});
			});

			it('after instantiation', function() {
				let size = 12;
				let numOfMines = 12;
				let board = new Board(size, numOfMines);

				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);

				board.updateSettings(zero, zero);
				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);

				board.updateSettings(negative, negative);
				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);

				board.updateSettings(stringNum, stringNum);
				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);

				board.updateSettings(fractional, fractional);
				assert.equal(board.size, size);
				assert.equal(board.numOfMines, numOfMines);

				assert.throws(() => {
					board.updateSettings(2, tooManyMines);
				});
			});
		});
	});

	describe('square guessing', function() {

		it('should not accept an out of bounds square', function() {
			let num = 1,
					negativeNum = -1,
					largeNum = 25;

			let pairs = [[num, negativeNum], [num, largeNum], [negativeNum, num], [largeNum, num]];

			for (let pair of pairs) {
				assert.throws(() => {
					board.guessSquare(pair[0], pair[1]);
				});
			}
		});

		describe('should update status of square', function() {
			let board, boardMock, sqMock, mineCoord;

			before(() => {
				sqMock = function(m) {
					this.setStatus = s => this.s = s;
					this.getStatus = () => this.s;
					this.isAMine = () => !!m;
					this.getMineCount = () => 1;
				};
				board = new Board(2, 1);
				boardMock = [[new sqMock(true), new sqMock()], [new sqMock(), new sqMock()]];
				board.board = boardMock;
				mineCoord = [1, 1];
			});

			it('with flag', function() {
				let coord = [2, 1];
				let status;
				board.guessSquare(coord[0], coord[1], 'F');
				status = boardMock[coord[0]-1][coord[1]-1].s;
				assert.equal(status, board.FLAGGED);

				board.guessSquare(coord[0], coord[1], 'Q');
				status = boardMock[coord[0]-1][coord[1]-1].s;
				assert.equal(status, board.QUESTION);

				board.guessSquare(coord[0], coord[1], 'P');
				status = boardMock[coord[0]-1][coord[1]-1].s;
				assert.equal(status, board.PRISTINE);
			});

			it('without flag', function() {
				let coord = [2, 1];
				let status;

				board.guessSquare(mineCoord[0], mineCoord[1]);
				status = boardMock[mineCoord[0]-1][mineCoord[1]-1].s;
				assert(status, board.EXPLODED);

				board.guessSquare(coord[0], coord[1]);
				status = boardMock[coord[0]-1][coord[1]-1].s;
				assert(status, board.REVEALED);
			});
		});
	});

	describe('end game', function() {
		let board, boardMock, sqMock;

		beforeEach(() => {
			sqMock = function(m) {
				this.setStatus = s => this.s = s;
				this.getStatus = () => this.s;
				this.isAMine = () => !!m;
				this.getMineCount = () => 1;
			};
			board = new Board(2, 1);
			boardMock = [[new sqMock(true), new sqMock()], [new sqMock(), new sqMock()]];
			board.board = boardMock;
		});

		it('should lose when mine found', function() {
			let retStr = board.checkBoardState(board.EXPLODED);
			assert(retStr && !retStr.match(/win/i));
		});

		it('should win when all non-mines revealed', function() {
			let retStr;

			retStr = board.guessSquare(1, 2);
			assert(!retStr);

			retStr = board.guessSquare(2, 1);
			assert(!retStr);

			retStr = board.guessSquare(2, 2);
			assert(retStr && retStr.match(/win/i));
		});

		it('should win when all mines and no non-mines flagged', function() {
			let retStr;

			retStr = board.guessSquare(1, 2, 'F');
			assert(!retStr);

			retStr = board.guessSquare(1, 1, 'F');
			assert(!retStr);

			retStr = board.guessSquare(1, 2, 'P');
			assert(retStr && retStr.match(/win/i));
		});
	});

	describe('printing', function() {
		it('should match a particular format');

		it('should handle all possible square statuses');
	});
});
