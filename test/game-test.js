'use strict';

const assert = require('assert');
const Game = require('../Game').Game;

describe('Game', function() {
	describe('settings', function() {
		let game;

		before(function() {
			game = new Game();
		});

		beforeEach(function() {
			let i = 0;
			game.questionAsPromise = question => Promise.resolve(game._testAnswers[i++]);
		});

		describe('should not accept invalid', function() {
			describe('size settings like', function() {
				let sizeAns, minesAns, defaultSize;

				before(function() {
					minesAns = '';
					defaultSize = 5;
				});

				afterEach(function(done) {
					game.updateSettings()
					.then(function() {
						assert.equal(game.boardSize, defaultSize);
						done();
					})
					.catch(done);
				});

				it('non-positive numbers', function() {
					sizeAns = '0';
					game._testAnswers = [sizeAns,minesAns];
				});

				it('fractional numbers', function() {
					sizeAns = '12.5';
					game._testAnswers = [sizeAns,minesAns];
				});

				it('non-numbers', function() {
					sizeAns = 'string';
					game._testAnswers = [sizeAns,minesAns];
				});
			});

			describe('mine settings like', function() {
				let sizeAns, minesAns, defaultMines;

				before(function() {
					sizeAns = '';
					defaultMines = 5;
				});

				afterEach(function(done) {
					game.updateSettings()
					.then(function() {
						assert.equal(game.numOfMines, defaultMines);
						done();
					})
					.catch(done);
				});

				it('non-positive numbers', function() {
					minesAns = '0';
					game._testAnswers = [sizeAns,minesAns];
				});

				it('fractional numbers', function() {
					minesAns = '12.5';
					game._testAnswers = [sizeAns,minesAns];
				});

				it('non-numbers', function() {
					minesAns = 'string';
					game._testAnswers = [sizeAns,minesAns];
				});

				it('too many mines', function() {
					minesAns = '25';
					game._testAnswers = [sizeAns,minesAns];
				});
			});
		});
	});

	describe('on user coordinate input', function() {
		let game;

		before(function() {
			game = new Game();
		});

		after(function() {
			game.endGame();
		});

		it('should accept proper format', function() {
			let colIn = 21,
					rowIn = 11;
			let answer = `${colIn},${rowIn}`;
			let { col, row, flag, quit } = game.parseAnswer(answer);

			assert.equal(col, colIn);
			assert.equal(row, rowIn);
			assert(!flag);
			assert(!quit);
		});

		it('should accept supported flags', function() {
			let colIn = 21,
					rowIn = 11,
					flags = ['f', 'u', 'q'];
			let f, answer, col, row, flag, quit;

			for (f of flags) {
				answer = `${colIn},${rowIn}${f}`;
				({ col, row, flag, quit } = game.parseAnswer(answer));

				assert.equal(col, colIn);
				assert.equal(row, rowIn);
				assert(flag);
				assert(!quit);
			}
		});

		it('should ignore unsupported flags', function() {
			let colIn = 21,
					rowIn = 11,
					flags = ['F', 'U', 'Q', 'm'];
			let f, answer, col, row, flag, quit;

			for (f of flags) {
				answer = `${colIn},${rowIn}${f}`;
				({ col, row, flag, quit } = game.parseAnswer(answer));

				assert.equal(col, colIn);
				assert.equal(row, rowIn);
				assert(!flag);
				assert(!quit);
			}
		});

		describe('should not accept improper formats, like', function() {
			let colIn, rowIn, flagIn, answer, col, row, flag, quit;

			before(function() {
				colIn = 21;
				rowIn = 11;
				flagIn = 'f';
			});

			afterEach(function() {
				({ col, row, flag, quit } = game.parseAnswer(answer));
				assert(!col);
				assert(!row);
				assert(!flag);
				assert(!quit);
			});

			it('missing column', function() {
				answer = `,${rowIn}${flagIn}`;
			});

			it('missing row', function() {
				answer = `${colIn},${flagIn}`;
			});

			it('flag first', function() {
				answer = `${flagIn}${colIn},${rowIn}`;
			});

			it('wrong separator', function() {
				answer = `${colIn};${rowIn}${flagIn}`;
			});

			it('too many arguments', function() {
				answer = `${colIn},${rowIn},${colIn}`;
			});
		});
	});
});
