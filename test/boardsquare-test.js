'use strict';

const assert = require('assert');
const BoardSquare = require('../BoardSquare').BoardSquare;

describe('Board Square', function() {
	it('should accept a default status', function() {
		let defaultStatus = 50;
		let square = new BoardSquare(defaultStatus);
		assert.equal(square.getStatus(), defaultStatus);
	});

	describe('is a mine', function() {
		it('should be able to be checked', function() {
			let square = new BoardSquare();
			square._isAMine = true;
			assert(square.isAMine());
		});

		describe('status should be updatable', function() {
			let square, zeroStatus, fractionalStatus, stringStatus, negativeStatus;

			before(function() {
				square = new BoardSquare();
				zeroStatus = 0;
				fractionalStatus = 2.5;
				stringStatus = 'string';
				negativeStatus = -1;
			});

			it('with any value', function() {
				square.setStatus(zeroStatus);
				assert.equal(square.getStatus(), zeroStatus);

				square.setStatus(fractionalStatus);
				assert.equal(square.getStatus(), fractionalStatus);

				square.setStatus(stringStatus);
				assert.equal(square.getStatus(), stringStatus);

				square.setStatus(negativeStatus);
				assert.equal(square.getStatus(), negativeStatus);
			});

			it('unless current status is negative', function() {
				assert.equal(square.getStatus(), negativeStatus);

				square.setStatus(fractionalStatus);
				assert(square.getStatus(), negativeStatus);
			});
		});
	});

	describe('number of mined neighbors', function() {
		it('should be able to be checked', function() {
			let square = new BoardSquare();
			let newCount = 20;
			square.count = newCount;
			assert.equal(square.getMineCount(), newCount);
		});

		it('should be updatable', function() {
			let square = new BoardSquare();
			let newCount = 20;
			square.count = newCount;

			square.increaseMineCount();

			assert.equal(square.getMineCount(), newCount + 1);
		});
	});
});
